# Superset Alerts



## Configuración de superset_config.py

- [ ] Agregar las siguientes líneas al archivo de configuración:

```
SCREENSHOT_LOCATE_WAIT = 100
SCREENSHOT_LOAD_WAIT = 600
```

- [ ] Agregar la configuración de SMTP:

```

SMTP_HOST = "Nombre de host"
SMTP_PORT = 465
SMTP_STARTTLS = False
SMTP_SSL_SERVER_AUTH = True
SMTP_SSL = True
SMTP_USER = "usuario smtp"
SMTP_PASSWORD = "contraseña smtp"
SMTP_MAIL_FROM = "correo de origen"

```
- [ ] Completar la siguiente configuración:

```
ALERT_REPORTS_NOTIFICATION_DRY_RUN = False
WEBDRIVER_BASEURL = "http://superset_app:8088"
# The base URL for the email report hyperlinks.
WEBDRIVER_BASEURL_USER_FRIENDLY = "https://superset.test.datasetbi.com/"

```
Notese que WEBDRIVER_BASEURL_USER_FRIENDLY debe ser la URL del origen de los reportes

## Configuración del archivo docker-compose:

- [ ] Crear carpeta build en la raíz del proyecto 
- [ ] Copiar Dockfile en esa carpeta
- [ ] Agregar como origen el Dockfile en el contenedor del worker

```
  superset-worker:
    build:
      context: ./build
      dockerfile: Dockerfile
    container_name: superset_worker

```

- [ ] Exponer el puerto 465

```
    ports:
      - 8088:8088
      - 465:465
```

## Documentación:

https://superset.apache.org/docs/configuration/alerts-reports


